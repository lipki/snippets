// inspire by Daniel Shiffman
// http://codingtra.in

//<script src="libraries/p5.js"></script>
//<script src="libraries/p5.dom.js"></script>

var r1;
var r2;
var m1;
var m2;
var a1 = Math.PI+0.0001;
var a2 = Math.PI+0.0001;
var a1_v = 0;
var a2_v = 0;
var g = 1;
var px1 = 0;
var py1 = 0;
var px2 = 0;
var py2 = 0;

var blackboard;

function setup() {
	createCanvas(windowWidth, windowHeight);

	r1 = r2 = height/2/3;
	m1 = m2 = height/30;
	a1 = Math.PI*random();
  a2 = Math.PI*random();

	stroke(255);
	fill(255);
	strokeWeight(2);

	blackboard = createGraphics(width, height);
	blackboard.background(0);
	blackboard.translate(width/2, height/2);

  but = createElement('button', 'random');
	but.elt.onclick = function () {
		a1 = Math.PI*random();
	  a2 = Math.PI*random();
		a1_v = a2_v = 0;
		px1 = py1 = px2 = py2 = 0;
		blackboard.fill(0);
		blackboard.rect(-width/2, -height/2, width, height);
	};
}

function draw() {

	image(blackboard, 0, 0, width, height);

	var num1 = -g * (2 * m1 + m2) * sin(a1);
	var num2 = -m2 * g * sin(a1-2*a2);
	var num3 = -2*sin(a1-a2)*m2;
	var num4 = a2_v*a2_v*r2+a1_v*a1_v*r1*cos(a1-a2);
	var den = r1 * (2*m1+m2-m2*cos(2*a1-2*a2));
	var a1_a = (num1 + num2 + num3*num4) / den;

	num1 = 2 * sin(a1-a2);
	num2 = (a1_v*a1_v*r1*(m1+m2));
	num3 = g * (m1 + m2) * cos(a1);
	num4 = a2_v*a2_v*r2*m2*cos(a1-a2);
	den = r2 * (2*m1+m2-m2*cos(2*a1-2*a2));
	var a2_a = (num1*(num2+num3+num4)) / den;

	translate(width/2, height/2);

	var x1 = r1 * sin(a1);
	var y1 = r1 * cos(a1);

	var x2 = x1 + r2 * sin(a2);
	var y2 = y1 + r2 * cos(a2);

	line(0, 0, x1, y1);
	ellipse(x1, y1, m1, m1);

	line(x1, y1, x2, y2);
	ellipse(x2, y2, m2, m2);

	a1_v += a1_a;
	a2_v += a2_a;
	a1 += a1_v;
	a2 += a2_v;

	a1_v *= 0.9999;
	a2_v *= 0.9999;

	if (px1 != 0) {
		blackboard.stroke(0, 255, 0);
		blackboard.line(px1, py1, x1, y1);
		blackboard.stroke(255, 0, 0);
		blackboard.line(px2, py2, x2, y2);
		blackboard.stroke(0);
		blackboard.fill(0, 0, 0, 3);
		blackboard.rect(-width/2, -height/2, width, height);
	}

	px1 = x1;
	py1 = y1;
	px2 = x2;
	py2 = y2;
}
